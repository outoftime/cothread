/* 简介：cothread 是一个轻量级协程调度器，由纯C语言实现，易于移植到各种单片机。
 * 同时，由于该调度器仅仅运行在一个实际线程中，所以它也适用于服务器高并发场景。
 *
 * 版本: 1.0.0   2019/02/25
 *
 * 作者: 覃攀 <qinpan1003@qq.com>
 *
 */

#ifndef _LOCK_H_
#define _LOCK_H_

typedef struct _mutex {
    int lock;
}mutex_t;

typedef unsigned int irq_state_t;

/* 这两个函数由驱动提供 */
irq_state_t irq_save_disable(void);
void irq_restore(irq_state_t);

#define cothread_critical_stat()\
    irq_state_t __unique_stat

#define cothread_enter_critical()\
    __unique_stat = irq_save_disable()

#define cothread_exit_critical()\
    irq_restore(__unique_stat)

/* 非抢占调度器，用不上这两个锁操作 */
void mutex_lock(mutex_t *mutex);
void mutex_unlock(mutex_t *mutex);

#endif // _LOCK_H_

